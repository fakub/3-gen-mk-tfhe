include("./src/TFHE.jl")
using Random, Printf
using .TFHE


function main()
    parties = 2

    params = mktfhe_parameters_2party_3gen

    rng = MersenneTwister()

    @printf("3rd MK-TFHE - %s parties\n========\n(3) KEY GENERATION AND PRECOMP ...\n",parties)

    secret_keys = [SecretKey_3gen(rng, params) for _ in 1:parties]

    rlwe_keys = [RLweKey(rng,rlwe_parameters(params),true) for _ in 1:parties]

    precomp_time = @elapsed crp_a = CRP_3gen(rng, tgsw_parameters(params), rlwe_parameters(params),true)

    precomp_time += @elapsed pubkeys=[PublicKey(rng,rlwe_keys[i],params.gsw_noise_stddev,crp_a,tgsw_parameters(params),1) for i=1:parties] # Generation of the individual public keys.

    precomp_time += @elapsed common_pubkey=CommonPubKey_3gen(pubkeys, params, parties)  #generation of the common public key.

    precomp_time += @elapsed bk_keys = [BootstrapKeyPart_3gen(rng, secret_keys[i].key, params.gsw_noise_stddev,crp_a,
                common_pubkey, tgsw_parameters(params), rlwe_parameters(params),1) for i in 1:parties]

    precomp_time+= @elapsed bk_keys=[TransformedBootstrapKeyPart_3gen(bk_keys[i]) for i in 1:parties]

    precomp_time+= @elapsed ks_keys = [KeyswitchKey(rng,params.ks_noise_stddev, keyswitch_parameters(params),secret_keys[i].key,rlwe_keys[i]) for i in 1:parties]



    @printf("(3) PRECOMP TIME : %s seconds",precomp_time)

    getsize(var) = Base.format_bytes(Base.summarysize(var)/parties)

    @printf("(3) BK SIZE : %s, KSK SIZE : %s\n\n", getsize(bk_keys), getsize(ks_keys))



    for trial=1:5
        mess1 = rand(Bool)
        mess2 = rand(Bool)
        out = !(mess1 && mess2)

        println(" \n\n(3) Trial - $trial: $mess1 NAND $mess2 = $out")

        enc_mess1 = mk_encrypt_3gen(rng, secret_keys, mess1)
        enc_mess2 = mk_encrypt_3gen(rng, secret_keys, mess2)

        dec_mess1 = mk_decrypt_3gen(secret_keys, enc_mess1)
        dec_mess2 = mk_decrypt_3gen(secret_keys, enc_mess2)

        @assert mess1 == dec_mess1
        @assert mess2 == dec_mess2

        print("NAND GATE TIME:")
            
        @time enc_out = mk_gate_nand_3gen(bk_keys,ks_keys,enc_mess1,enc_mess2)

        dec_out = mk_decrypt_3gen(secret_keys, enc_out)
        @assert out == dec_out

        println("Successful")
    
    end
end


main()