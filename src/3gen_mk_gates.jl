"""
*This File contains the functions that run given gates homomorphically for the 3rd generation of MKTFHE.
"""

"""Function that computes the NAND gate between x and y using the 3rd generation of MKTFHE"""
function mk_gate_nand_3gen(bk::Array{TransformedBootstrapKeyPart_3gen, 1}, ks::Array{KeyswitchKey, 1}, x::MKLweSample, y::MKLweSample)
    temp = (
        mk_lwe_noiseless_trivial(encode_message(1, 8), x.params, length(bk))
        - x - y)
    bk[1].rlwe_params.is32 ? encode = encode_message : encode = encode_message64
    mk_bootstrap_3gen(bk,ks, encode(1, 8), temp)
end
